﻿using UnityEngine;
using System.Collections;

public class RoomTemplate : MonoBehaviour {

	public Exit[] exits;
	
	[HideInInspector] public bool pathRoom = false;
	[HideInInspector] public int index = -1;
	
	// --------------- Custom Classes ---------------
	
	[System.Serializable]
	public class Exit {
		public Vector3 position;
		public DugeonGenerator.Direction direction;
		public Vector2 size;
		public GameObject doorPrefab;
		
		[HideInInspector] public DugeonGenerator.ExitStatus status = DugeonGenerator.ExitStatus.open;
		
		int overlapStrikes = 0;
		public void IncrementStrikes (int _maxStrikes) {
			overlapStrikes ++;
			if (overlapStrikes >= _maxStrikes) {
				status = DugeonGenerator.ExitStatus.condemned;
			}
		}
		
		public Exit(Vector3 _position, DugeonGenerator.Direction _direction, Vector2 _size){
			position = _position;
			direction = _direction;
			size = _size;
		}
	}
}
