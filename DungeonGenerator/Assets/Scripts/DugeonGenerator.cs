﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO Error handling if no source/destination node candidates
// TODO Replace for() by foreach() where possible?
// TODO Use seed (PRNG) fro randomization
// TODO Place Props...
// TODO Some functions probably can go private...
// TODO Some variables probably can go private...

public class DugeonGenerator : MonoBehaviour {

	public bool drawDebugGizmos;

	public int roomPathMax;
	public int roomLevelMax;
	public int iterationMax;
	

	public int maxOverlapStrikes;
	public float overlapDetectionRange;
	public RoomTemplate startingRoom;
	public RoomTemplate[] roomTemplates;

	private int iterationCount = 0;
	
	private List<RoomTemplate> rooms = new List<RoomTemplate>();
	private int startRoomIndex;
	private int finishRoomIndex;
	
	private GameObject dungeonContainer;

	public enum Direction {north, east, south, west};
	public enum ExitStatus {open, connected, condemned};
	
		
	void Start(){
	
		GenerateNewDungeon();

	}
	
	
	void Update() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			DestroyDungeon();
			GenerateNewDungeon();
		}
	}
	
	
	void OnDrawGizmos() {
	
		if (drawDebugGizmos) {

			for (int i = 0; i < rooms.Count; i++){
			
				Gizmos.color = (rooms[i].pathRoom)? Color.cyan:Color.yellow;
				Gizmos.DrawWireCube(rooms[i].collider.bounds.center, rooms[i].collider.bounds.size);
						
				Gizmos.color = Color.blue;
				Gizmos.DrawWireCube(rooms[startRoomIndex].collider.bounds.center, rooms[startRoomIndex].collider.bounds.size * 1.15f);
				
				Gizmos.color = Color.magenta;
				Gizmos.DrawWireCube(rooms[finishRoomIndex].collider.bounds.center, rooms[finishRoomIndex].collider.bounds.size * 1.15f);
				
	
		
				for (int j = 0; j < rooms[i].exits.Length; j++){
				
					switch (rooms[i].exits[j].status){
					case ExitStatus.open:
						Gizmos.color = Color.green;
						break;
					case ExitStatus.connected:
						Gizmos.color = Color.red;
						break;
					case ExitStatus.condemned:
						Gizmos.color = Color.black;
						break;
					default: 
						Gizmos.color = Color.white;
						break;
					}
				
					Gizmos.DrawWireCube(rooms[i].transform.position + rooms[i].exits[j].position, Vector3.one * 0.5f);
				}
			}
		
		}
	}
	



	
	// --------------- Custom functions ---------------
	
	public void GenerateNewDungeon() {
	
		float startTime = Time.realtimeSinceStartup;
	
		Debug.Log("-------------------------------");
		Debug.Log("Generating new dungeon...");
	
		dungeonContainer = new GameObject();
		dungeonContainer.name = "DungeonContainer";
		
		PlaceFirstRoom();
		startRoomIndex = rooms.Count - 1; // Will always be 0, but meh.
		
		while ((rooms.Count <= roomPathMax) && (iterationCount <= iterationMax)) {PlaceRandomRoom(true);}
		finishRoomIndex = rooms.Count - 1;
		CondemnRoomOpenExits(finishRoomIndex);
		
		while ((rooms.Count <= roomLevelMax) && (iterationCount <= iterationMax)) {PlaceRandomRoom(false);}
		CondemnAllOpenExits();
		
		CloseAllCondemedExits();
		
		Debug.Log("Completed generation!");
		Debug.Log("Rooms: " + (rooms.Count - 1).ToString());
		Debug.Log("Iterations: " + (iterationCount - 1).ToString());
		Debug.Log("Time taken: " + (Time.realtimeSinceStartup - startTime).ToString() + "s");
		Debug.Log("-------------------------------");
		
	}
	
	
		
	public void DestroyDungeon() {
		
		iterationCount = 0;
		startRoomIndex = 0;
		finishRoomIndex = 0;
		rooms = new List<RoomTemplate>();
		GameObject.DestroyImmediate(dungeonContainer);

	}
	
	public void PlaceFirstRoom(){
		RoomTemplate newRoom = (RoomTemplate)Instantiate(startingRoom, new Vector3(0,0,0), Quaternion.identity);
		newRoom.pathRoom = true;
		newRoom.transform.parent = dungeonContainer.transform;
		newRoom.index = rooms.Count;  // => rooms.Count + 1 - 1 ...
		rooms.Add(newRoom);
	}
	
	
		
	public void PlaceRandomRoom (bool _pathRoom){
		
		iterationCount ++;

		SourceNode sourceNode = null;		
		if (_pathRoom) {		
			int roomIndex = rooms.Count - 1;
			while ((sourceNode == null) && (roomIndex >= 0)) {
				sourceNode = SelectSourceNodeFromRoom(roomIndex);
				roomIndex --;
			}
		} else {
			sourceNode = SelectSourceNodeFromRoom(-1);
		}
		
		DestinationNode destinationNode = SelectDestinationNode(sourceNode);
		
		PlaceRoom(sourceNode, destinationNode, _pathRoom);
	}	
	


	public SourceNode SelectSourceNodeFromRoom(int _roomIndex){
	
		
		List<SourceNode> sourceNodeCandidates = new List<SourceNode>();

		// If roomIndex < 0 then pick form any room. Else use rooms[roomIndex].		
		if (_roomIndex < 0) {
			for (int i = 0; i < rooms.Count; i++){
				for (int j = 0; j < rooms[i].exits.Length; j++){
					if (rooms[i].exits[j].status == ExitStatus.open){
						sourceNodeCandidates.Add(new SourceNode(i,j));
					}
				}
			}
		} else {
			for (int j = 0; j < rooms[_roomIndex].exits.Length; j++){

				if (rooms[_roomIndex].exits[j].status == ExitStatus.open){
					sourceNodeCandidates.Add(new SourceNode(_roomIndex,j));
				}
			}
		}
	
	
		if (sourceNodeCandidates.Count > 0) {
			return sourceNodeCandidates[Random.Range(0, sourceNodeCandidates.Count)];
		} else {
			return null;
		}
	}
	
	
	
	public DestinationNode SelectDestinationNode (SourceNode _sourceNode){
						
		Direction destinationExitDirection  = OppositeDirection(rooms[_sourceNode.roomIndex].exits[_sourceNode.exitIndex].direction);
		
		List<DestinationNode> potentialRooms = new List<DestinationNode>();
		for (int i = 0; i < roomTemplates.Length; i++){
			for (int j = 0; j < roomTemplates[i].exits.Length; j++){
				if (destinationExitDirection == roomTemplates[i].exits[j].direction) {
					potentialRooms.Add(new DestinationNode(roomTemplates[i],j));
				}
			}
		}

		if (potentialRooms.Count > 0) {
			return potentialRooms[Random.Range(0, potentialRooms.Count)];
		} else {
			return null;
		}
				
	}	



	public void PlaceRoom(SourceNode _sourceNode, DestinationNode _destinationNode, bool _pathRoom){
	
		// Calculate room position
		Vector3 newRoomPosition = rooms[_sourceNode.roomIndex].transform.position + rooms[_sourceNode.roomIndex].exits[_sourceNode.exitIndex].position - _destinationNode.roomTemplate.exits[_destinationNode.exitIndex].position;

		// List rooms in range
		Collider[] roomsInRange = Physics.OverlapSphere(newRoomPosition, overlapDetectionRange);
		
		// Create the room
		RoomTemplate newRoom = (RoomTemplate)Instantiate(_destinationNode.roomTemplate, newRoomPosition, Quaternion.identity);
		newRoom.pathRoom = _pathRoom;
		
		if (RoomOverlaps(newRoom.collider, roomsInRange)) {
			
			DestroyImmediate(newRoom.gameObject);
			rooms[_sourceNode.roomIndex].exits[_sourceNode.exitIndex].IncrementStrikes(maxOverlapStrikes);
				
		} else {
		
			// Closing source and destination connectors
			newRoom.exits[_destinationNode.exitIndex].status = ExitStatus.connected;
			rooms[_sourceNode.roomIndex].exits[_sourceNode.exitIndex].status = ExitStatus.connected; 
			
			// Check & close extra overlapping connectors
			for (int i = 0; i < roomsInRange.Length; i++){
				int roomInRangeIndex = roomsInRange[i].gameObject.GetComponent<RoomTemplate>().index;
				for (int j = 0; j < rooms[roomInRangeIndex].exits.Length; j++){
					for (int k = 0; k < newRoom.exits.Length; k++) {
						
						Vector3 connectorPositionA = newRoom.transform.position + newRoom.exits[k].position;
						Vector3 connectorPositionB = rooms[roomInRangeIndex].transform.position + rooms[roomInRangeIndex].exits[j].position;
						
						if ((rooms[roomInRangeIndex].exits[j].status == ExitStatus.open) && (newRoom.exits[k].status == ExitStatus.open)  && (connectorPositionA == connectorPositionB)){
							rooms[roomInRangeIndex].exits[j].status = ExitStatus.connected;
							newRoom.exits[k].status = ExitStatus.connected;
						}	
					}
				}
			}
			newRoom.transform.parent = dungeonContainer.transform;
			newRoom.index = rooms.Count;  // => rooms.Count + 1 - 1 ...
			rooms.Add(newRoom);
		}
	}
	
	
	public void CondemnRoomOpenExits(int _roomIndex) {
		for (int i = 0; i < rooms[_roomIndex].exits.Length; i++) {
			if (rooms[_roomIndex].exits[i].status == ExitStatus.open) {
				rooms[_roomIndex].exits[i].status = ExitStatus.condemned;
			}
		}
	}
	
	public void CondemnAllOpenExits() {
		for (int i = 0; i < rooms.Count; i++){
			CondemnRoomOpenExits(i);
		}
	}
	
	
	public void CloseAllCondemedExits() {
		for (int i = 0; i < rooms.Count; i++){
			for (int j = 0; j < rooms[i].exits.Length; j++){
				if (rooms[i].exits[j].status == ExitStatus.condemned){
					GameObject newDoor = (GameObject)Instantiate(rooms[i].exits[j].doorPrefab, rooms[i].transform.position + rooms[i].exits[j].position, Quaternion.identity);
					newDoor.transform.parent = rooms[i].transform;
				}
			}
		}
	}
	

	public Direction OppositeDirection(Direction _direction) {
	
		switch (_direction){
		case Direction.north:
			return Direction.south;
		case Direction.east:
			return Direction.west;
		case Direction.south:
			return Direction.north;
		default: // Only west => east remains, setting it as default.
			return Direction.east;		
		}
	}

	
	public bool RoomOverlaps(Collider _roomToCheck, Collider[] _roomsInRange){
	
		Bounds overlapBounds = new Bounds(_roomToCheck.collider.bounds.center, _roomToCheck.collider.bounds.extents * 2f * 0.99f);
	
		for (int i = 0; i < _roomsInRange.Length; i++){
			if (overlapBounds.Intersects(_roomsInRange[i].bounds)){
				return true;
			}
		}
		return false;
	}
	



	// --------------- Custom Classes ---------------
	
	public class DestinationNode {
		public RoomTemplate roomTemplate;
		public int exitIndex;
				
		public DestinationNode(RoomTemplate _roomTemplate, int _exitIndex){
			roomTemplate = _roomTemplate;
			exitIndex = _exitIndex;
		}
	}
	
	public class SourceNode {
		public int roomIndex;
		public int exitIndex;
		
		public SourceNode(int _roomIndex, int _exitIndex){
			roomIndex = _roomIndex;
			exitIndex = _exitIndex;
		}
	}
	
}

